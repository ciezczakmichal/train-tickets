const express = require('express')
const morgan = require('morgan')
require('moment-timezone')

const path = require('path')

const app = express()

app.use(morgan('tiny'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// CORS
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    )
    next()
})

app.use(require('./routes/index.routes'))

app.use('/api/static', express.static(path.join(__dirname, 'static')))

app.listen('8100')
