// baza użytkowników podczas wykonywania
const usersDB = []

// baza sesji użytkowników
const sessionsDB = []

// baza biletów użytkowników
const ticketsDB = []

// inicjalizuj wartościami poczatkowymi
const hardcodedUsers = require('../data/users.json')
const hardcodedTickets = require('../data/tickets.json')

hardcodedUsers.forEach(user => {
    usersDB.push(user)
})

hardcodedTickets.forEach(ticket => {
    ticketsDB.push(ticket)
})

let nextUserID = usersDB.length > 0 ? usersDB[usersDB.length - 1].id + 1 : 1
let nextTicketID =
    ticketsDB.length > 0 ? ticketsDB[ticketsDB.length - 1].id + 1 : 1

module.exports = {
    usersDB,
    sessionsDB,
    ticketsDB,
    nextUserID,
    nextTicketID,
}
