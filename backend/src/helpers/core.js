const trainsDB = require('../data/trains.json')

const moment = require('moment')

const dateFormat = 'YYYY-MM-DD'
const timeFormat = 'HH:mm'
const responseFormat = `${dateFormat} ${timeFormat}`

const idDateFormat = 'YYYYMMDD'

// konwersja dat i czasu na obiekty
function parseTrainRoutes(train) {
    train.route.forEach(entry => {
        if (entry.arrivalTime !== null) {
            entry.arrivalTime = moment(entry.arrivalTime, timeFormat)
        }

        if (entry.departureTime !== null) {
            entry.departureTime = moment(entry.departureTime, timeFormat)
        }
    })
}

// dzień jako obiekt moment()
function getTrainsForDay(day) {
    return trainsDB.map(train => {
        const newObj = JSON.parse(JSON.stringify(train))
        parseTrainRoutes(newObj)

        // unikalna właściwość id
        newObj.id = day.format(idDateFormat) + '_' + newObj.trainID

        // aktualizuje właściwości o zapytaną datę
        newObj.route.forEach(entry => {
            if (entry.arrivalTime !== null) {
                const d = day.clone()
                d.hours(entry.arrivalTime.hours())
                d.minutes(entry.arrivalTime.minutes())
                entry.arrivalTime = d
            }

            if (entry.departureTime !== null) {
                const d = day.clone()
                d.hours(entry.departureTime.hours())
                d.minutes(entry.departureTime.minutes())
                entry.departureTime = d
            }
        })

        return newObj
    })
}

function getTrainByGlobalId(trainGlobalID) {
    // zdekoduj datę, która obecna jest w identyfikatorze
    const datePart = trainGlobalID.split('_')
    const date = moment(datePart, idDateFormat)

    let result = getTrainsForDay(date).filter(
        train => train.id === trainGlobalID
    )
    if (result.length !== 1) {
        result = null
    } else {
        result = result[0]
    }

    return result
}

module.exports = {
    dateFormat,
    timeFormat,
    responseFormat,
    parseTrainRoutes,
    getTrainsForDay,
    getTrainByGlobalId,
}
