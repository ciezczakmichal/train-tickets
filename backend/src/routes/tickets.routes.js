const express = require('express')
const router = express.Router()
const tickets = require('../models/tickets.model')

router.get('/forUser/:token', async (req, res) => {
    await tickets
        .getUserTickets(req.params.token)
        .then(data => res.json(data))
        .catch(err => {
            if (err.status) {
                res.status(err.status).json({ message: err.message })
            } else {
                res.status(500).json({ message: err.message })
            }
        })
})

router.get('/:ticketID/qr', async (req, res) => {
    await tickets
        .getTicketQRData(req.body)
        .then(data => res.json(data))
        .catch(err => {
            if (err.status) {
                res.status(err.status).json({ message: err.message })
            } else {
                res.status(500).json({ message: err.message })
            }
        })
})

router.post('/buy', async (req, res) => {
    await tickets
        .buyTicket(req.body)
        .then(data => res.json(data))
        .catch(err => {
            if (err.status) {
                res.status(err.status).json({ message: err.message })
            } else {
                res.status(500).json({ message: err.message })
            }
        })
})

module.exports = router
