const express = require('express')
const router = express.Router()

router.use('/api/train-categories', require('./trainCategories.routes'))
router.use('/api/stations', require('./stations.routes'))
router.use('/api/news', require('./news.routes'))

router.use('/api/searchTrains', require('./searchTrains.routes'))
router.use('/api/tickets', require('./tickets.routes'))
router.use('/api/users', require('./users.routes'))

module.exports = router
