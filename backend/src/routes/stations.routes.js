const express = require('express')
const router = express.Router()
const stations = require('../models/stations.model')

router.get('/', async (req, res) => {
    await stations
        .getStations()
        .then(data => res.json(data))
        .catch(err => {
            if (err.status) {
                res.status(err.status).json({ message: err.message })
            } else {
                res.status(500).json({ message: err.message })
            }
        })
})

module.exports = router
