const express = require('express')
const router = express.Router()
const users = require('../models/users.model')

router.post('/signIn', async (req, res) => {
    await users
        .signInUser(req.body)
        .then(data => res.json(data))
        .catch(err => {
            if (err.status) {
                res.status(err.status).json({ message: err.message })
            } else {
                res.status(500).json({ message: err.message })
            }
        })
})

router.post('/signUp', async (req, res) => {
    await users
        .signUpUser(req.body)
        .then(data => res.json(data))
        .catch(err => {
            if (err.status) {
                res.status(err.status).json({ message: err.message })
            } else {
                res.status(500).json({ message: err.message })
            }
        })
})

router.post('/logOut', async (req, res) => {
    await users
        .logOut(req.body)
        .then(data => res.json(data))
        .catch(err => {
            if (err.status) {
                res.status(err.status).json({ message: err.message })
            } else {
                res.status(500).json({ message: err.message })
            }
        })
})

module.exports = router
