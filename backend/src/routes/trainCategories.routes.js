const express = require('express')
const router = express.Router()
const trainCategories = require('../models/trainCategories.model')

router.get('/', async (req, res) => {
    await trainCategories
        .getCategories()
        .then(categories => res.json(categories))
        .catch(err => {
            if (err.status) {
                res.status(err.status).json({ message: err.message })
            } else {
                res.status(500).json({ message: err.message })
            }
        })
})

module.exports = router
