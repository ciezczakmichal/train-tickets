const express = require('express')
const router = express.Router()
const searchTrains = require('../models/searchTrains.model')

router.post('/', async (req, res) => {
    await searchTrains
        .searchTrains(req.body)
        .then(data => res.json(data))
        .catch(err => {
            if (err.status) {
                res.status(err.status).json({ message: err.message })
            } else {
                res.status(500).json({ message: err.message })
            }
        })
})

module.exports = router
