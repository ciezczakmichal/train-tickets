const express = require('express')
const router = express.Router()
const news = require('../models/news.model')

router.get('/', async (req, res) => {
    await news
        .getNews()
        .then(data => res.json(data))
        .catch(err => {
            if (err.status) {
                res.status(err.status).json({ message: err.message })
            } else {
                res.status(500).json({ message: err.message })
            }
        })
})

module.exports = router
