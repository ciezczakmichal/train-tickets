const fs = require('fs')
const path = require('path')

const db = require('../helpers/db')
const core = require('../helpers/core')

const moment = require('moment')

function getUserIDByToken(token) {
    const session = db.sessionsDB.find(session => session.token === token)
    return session === undefined ? null : session.userID
}

function getTrainRouteEntry(trainData, stationID) {
    return trainData.route.find(entry => entry.stationID === stationID) || null
}

function getUserTickets(userToken) {
    return new Promise((resolve, reject) => {
        const response = []
        const userID = getUserIDByToken(userToken)

        if (userID !== null) {
            const userTickets = db.ticketsDB.filter(
                ticket => ticket.userID === userID
            )

            userTickets.forEach(ticket => {
                const trainData = core.getTrainByGlobalId(ticket.trainGlobalID)

                // znajdź datę odjazdu
                const departureEntry = getTrainRouteEntry(
                    trainData,
                    ticket.stationFromID
                )

                if (trainData !== null && departureEntry !== null) {
                    response.push({
                        id: ticket.id,
                        purchaseTime: ticket.purchaseTime,
                        stationFromID: ticket.stationFromID,
                        stationToID: ticket.stationToID,
                        trainCategoryID: trainData.trainCategoryID,
                        departureTime: departureEntry.departureTime.tz(
                            'Europe/Warsaw',
                            true
                        ),
                    })
                }
            })
        }

        resolve(response)
    })
}

function getTicketQRData(params) {
    return new Promise((resolve, reject) => {
        const data = fs.readFileSync(path.join(__dirname, '../static/qr.png'))
        const base64data = data.toString('base64')

        resolve(base64data)
    })
}

function buyTicket(params) {
    return new Promise((resolve, reject) => {
        const userID = getUserIDByToken(params.token)
        let response = false

        if (userID !== null) {
            const trainData = core.getTrainByGlobalId(params.entryID)

            // test, czy pociąg zawiera wskazane stacje
            const departureEntry = getTrainRouteEntry(
                trainData,
                params.stationFromID
            )
            const arrivalEntry = getTrainRouteEntry(
                trainData,
                params.stationToID
            )

            if (departureEntry !== null && arrivalEntry !== null) {
                db.ticketsDB.push({
                    id: db.nextTicketID,
                    userID: userID,
                    purchaseTime: moment()
                        .tz('Europe/Warsaw')
                        .format(core.responseFormat),
                    stationFromID: params.stationFromID,
                    stationToID: params.stationToID,
                    trainGlobalID: params.entryID,
                })

                db.nextTicketID++

                response = true
            }
        }

        resolve(response)
    })
}

module.exports = {
    getUserTickets,
    getTicketQRData,
    buyTicket,
}
