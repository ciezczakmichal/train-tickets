const db = require('../helpers/db')
const crypto = require('crypto')

function generateRandomString() {
    return crypto.randomBytes(20).toString('hex')
}

function getUserByEmail(email) {
    return db.usersDB.find(user => user.email === email) || null
}

function getUserById(id) {
    return db.usersDB.find(user => user.id === id) || null
}

function generateUserToken(userID) {
    const user = getUserById(userID)
    let token = null

    if (user !== null) {
        token = generateRandomString()
        db.sessionsDB.push({
            userID,
            token,
        })
    }

    return token
}

function removeUserToken(token) {
    const index = db.sessionsDB.findIndex(session => session.token === token)
    const index_valid = index > -1
    if (index_valid) {
        db.sessionsDB.splice(index, 1)
    }
    return index_valid
}

function signInUser(params) {
    return new Promise((resolve, reject) => {
        const user = getUserByEmail(params.email)
        let token = null

        // jeśli użytkownik o takim email istnieje i hasło się zgadza
        if (user !== null && user.password === params.password) {
            token = generateUserToken(user.id)
        }

        resolve(token)
    })
}

function signUpUser(params) {
    return new Promise((resolve, reject) => {
        const data = {
            id: db.nextUserID,
            email: params.email,
            password: params.password,
        }

        db.nextUserID++

        let token = null

        // jeśli użytkownik o takim email nie istnieje
        if (getUserByEmail(data.email) === null) {
            db.usersDB.push(data)
            token = generateUserToken(data.id)
        }

        resolve(token)
    })
}

function logOut(params) {
    return new Promise((resolve, reject) => {
        const response = removeUserToken(params.token)
        resolve(response)
    })
}

module.exports = {
    signInUser,
    signUpUser,
    logOut,
}
