const trainCategories = require('../data/trainCategories.json')

function getCategories() {
    return new Promise((resolve, reject) => {
        if (trainCategories.length === 0) {
            reject({
                message: 'Brak kategorii pociągów',
                status: 202,
            })
        }

        resolve(trainCategories)
    })
}

module.exports = {
    getCategories,
}
