const stations = require('../data/stations.json')

function getStations() {
    return new Promise((resolve, reject) => {
        if (stations.length === 0) {
            reject({
                message: 'Brak danych o stacjach',
                status: 202,
            })
        }

        resolve(stations)
    })
}

module.exports = {
    getStations,
}
