const core = require('../helpers/core')
const moment = require('moment')

function countTicketPrice(trainCategoryID, distance) {
    return Math.max(
        trainCategoryID === 1 ? distance * 0.25 : distance * 0.17,
        1
    ).toPrecision(3)
}

function filterTrains(params) {
    // konwertuj parametry
    params.date = moment(params.date, core.dateFormat)
    params.time = moment(params.time, core.timeFormat)

    // wyszukiwana data i czas
    const searchDateTime = params.date.clone()
    searchDateTime.hours(params.time.hours())
    searchDateTime.minutes(params.time.minutes())

    // zwróć pociągi na dany dzień
    let trains = core.getTrainsForDay(params.date)

    // wyfiltruj pożądane kategorie
    trains = trains.filter(train =>
        params.trainCategories.includes(train.trainCategoryID)
    )

    // główne filtrowanie
    trains = trains.filter(train => {
        let departureEntry = null
        let arrivalEntry = null

        for (const entry of train.route) {
            if (entry.stationID === params.stationFromID) {
                departureEntry = entry
            }

            if (entry.stationID === params.stationToID) {
                arrivalEntry = entry
            }

            // stacja przyjazdu nie może być znaleziona wcześniej
            if (arrivalEntry !== null && departureEntry === null) {
                return false
            }
        }

        // czy znaleziono stacje
        let result = arrivalEntry !== null && departureEntry !== null

        // czy godziny się zgadzają (czas wyjazdu lub przyjazdu)
        if (params.departure) {
            result =
                result &&
                departureEntry.departureTime.isSameOrAfter(searchDateTime)
        } else {
            result =
                result &&
                arrivalEntry.arrivalTime.isSameOrBefore(searchDateTime)
        }

        // jeśli pociąg się kwalifikuje, przypisz dodatkowe właściwości
        if (result) {
            // wynikowy czas odjazdu
            train.resultDeparture = departureEntry.departureTime

            // wynikowy czas przyjazdu
            train.resultArrival = arrivalEntry.arrivalTime

            // czas podróży
            train.resultTravelTime = train.resultArrival.diff(
                train.resultDeparture,
                'minutes'
            )

            // cena biletu
            const distance = arrivalEntry.distance - departureEntry.distance
            train.resultTicketPrice = countTicketPrice(
                train.trainCategoryID,
                distance
            )

            // czy można nabyć (do 5 minut przed odjazdem)
            train.resultPurchasable = moment()
                .add(5, 'minutes')
                .isSameOrBefore(departureEntry.departureTime)
        }

        return result
    })

    return trains
}

function searchTrains(params) {
    return new Promise((resolve, reject) => {
        const trains = filterTrains(params)
        const response = {
            params: {
                stationFromID: params.stationFromID,
                stationToID: params.stationToID,
            },
            results: [],
        }

        // sortuj wyniki według czasu odjazdu
        trains.sort((a, b) =>
            a.resultDeparture.isSameOrBefore(b.resultDeparture) ? -1 : 1
        )

        trains.forEach(train => {
            response.results.push({
                id: train.id,
                departureTime: train.resultDeparture.format(
                    core.responseFormat
                ),
                arrivalTime: train.resultArrival.format(core.responseFormat),
                routeInfo: {
                    trainCatID: train.trainCategoryID,
                    trainID: train.trainID,
                },
                travelTime: train.resultTravelTime,
                price: train.resultTicketPrice,
                purchasable: train.resultPurchasable,
            })
        })

        resolve(response)
    })
}

module.exports = {
    searchTrains,
}
