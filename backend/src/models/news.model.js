const news = require('../data/news.json')

function getNews() {
    return new Promise((resolve, reject) => {
        if (news.length === 0) {
            reject({
                message: 'Brak artykułów',
                status: 202,
            })
        }

        resolve(news)
    })
}

module.exports = {
    getNews,
}
