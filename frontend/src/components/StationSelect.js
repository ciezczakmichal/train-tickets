import React from 'react'
import Autosuggest from 'react-autosuggest'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleRight } from '@fortawesome/free-solid-svg-icons'
import './StationSelect.scss'

const replacePolishChars = s => {
    s = s.replace(/ę/gi, 'e')
    s = s.replace(/ż/gi, 'z')
    s = s.replace(/ó/gi, 'o')
    s = s.replace(/ł/gi, 'l')
    s = s.replace(/ć/gi, 'c')
    s = s.replace(/ś/gi, 's')
    s = s.replace(/ź/gi, 'z')
    s = s.replace(/ń/gi, 'n')
    s = s.replace(/ą/gi, 'a')
    return s
}

const prepareStationName = displayName =>
    replacePolishChars(displayName.toLowerCase())

const stationNameMatchedExactly = (userValue, station) => {
    return (
        prepareStationName(userValue) ===
        prepareStationName(station.displayName)
    )
}

const getSuggestionValue = suggestion => suggestion.displayName

const renderSuggestion = suggestion => (
    <div className="station">
        <FontAwesomeIcon icon={faAngleRight} />
        <div className="name">{suggestion.displayName}</div>
    </div>
)

export class StationSelect extends React.Component {
    static defaultProps = {
        stationId: null,
    }

    state = {
        stationId: null, // id bieżącej stacji lub null
        textValue: '', // tekst aktualnie wpisany przez użytkownika
        suggestions: [], // lista aktualnie wyświetlanych sugesti (obiekty)
    }

    updateInputValueFromProps = () => {
        const { stationId } = this.props

        let textValue = ''

        if (stationId !== null) {
            const station = this.props.stations.find(
                station => station.id === stationId
            )
            textValue = station ? getSuggestionValue(station) : ''
        }

        this.setState({ textValue })
    }

    getSuggestions = value => {
        const inputValue = prepareStationName(value.trim())
        const inputLength = inputValue.length

        if (inputLength < 3) {
            return [] // min 3 znaki wpisane
        } else {
            const result = this.props.stations
                .filter(station =>
                    new RegExp(inputValue).test(
                        prepareStationName(station.name)
                    )
                )
                .sort((a, b) => {
                    /* Sortuj według popularności stacji, chyba że dopasowywany
                     * jest dokładnie początek nazwy - wówczas te preferuj
                     * (np. ław -> Ławica, Wrocław Główny */
                    const aStarts = prepareStationName(a.name).startsWith(
                        inputValue
                    )
                    const bStarts = prepareStationName(b.name).startsWith(
                        inputValue
                    )

                    if (aStarts !== bStarts) {
                        return aStarts ? -1 : 1
                    } else {
                        return b.popularity - a.popularity
                    }
                })
                .splice(0, 5) // pokaż do 5 wyników

            // nie pokazuj podpowiedzi, gdy dokładna fraza wpisana i nie ma innych wyników
            return result.length === 1 &&
                stationNameMatchedExactly(inputValue, result[0])
                ? []
                : result
        }
    }

    onChange = (event, { newValue }) => {
        const station = this.props.stations.find(station =>
            stationNameMatchedExactly(newValue, station)
        )
        const stationId = station === undefined ? null : station.id

        this.setState({
            stationId,
            textValue: newValue,
        })

        if (this.props.onChange && this.state.stationId !== stationId) {
            this.props.onChange(stationId)
        }
    }

    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: this.getSuggestions(value),
        })
    }

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: [],
        })
    }

    componentDidMount() {
        this.setState({
            stationId: this.props.stationId,
        })
        this.updateInputValueFromProps()
    }

    componentDidUpdate() {
        if (this.props.stationId !== this.state.stationId) {
            this.setState({
                stationId: this.props.stationId,
            })
            this.updateInputValueFromProps()
        }
    }

    render() {
        const { stationId, textValue, suggestions } = this.state

        // Autosuggest will pass through all these props to the input.
        const inputProps = {
            placeholder: this.props.placeholder,
            value: textValue,
            onChange: this.onChange,
            spellCheck: 'false',
            title: this.props.title,
        }

        const invalidValueClass =
            (stationId === null && textValue !== '') || this.props.invalid
                ? 'invalid-value'
                : ''

        return (
            <div className={`${this.props.className} ${invalidValueClass}`}>
                <Autosuggest
                    suggestions={suggestions}
                    onSuggestionsFetchRequested={
                        this.onSuggestionsFetchRequested
                    }
                    onSuggestionsClearRequested={
                        this.onSuggestionsClearRequested
                    }
                    getSuggestionValue={getSuggestionValue}
                    renderSuggestion={renderSuggestion}
                    inputProps={inputProps}
                />
            </div>
        )
    }
}
