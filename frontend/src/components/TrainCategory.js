import React from 'react'
import './TrainCategory.scss'

export const TrainCategory = props => {
    const {
        selectable = true,
        selected = true,
        category,
        onSelected = () => {},
    } = props
    const { title, display_name, color } = category

    let className = 'train-category'

    if (selectable) {
        className += ' selectable'
    }

    if (selected) {
        className += ' selected'
    }

    const handleClick = () => {
        if (selectable) {
            onSelected(!selected)
        }
    }

    return (
        // eslint-disable-next-line jsx-a11y/anchor-is-valid
        <a
            className={className}
            style={{ backgroundColor: color }}
            title={title}
            onClick={handleClick}
        >
            {display_name}
        </a>
    )
}
