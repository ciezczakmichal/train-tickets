import React from 'react'
import { Link } from 'react-router-dom'
import './Footer.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'
import {
    faFacebookF,
    faTwitter,
    faInstagram,
} from '@fortawesome/free-brands-svg-icons'

const ListItem = ({ children }) => (
    <li className="footer-list-item">
        <span className="icon">
            <FontAwesomeIcon icon={faChevronRight} />
        </span>
        <span className="text">{children}</span>
    </li>
)

const ListItemLink = ({ children }) => (
    <ListItem>
        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
        <a href="#">{children}</a>
    </ListItem>
)

export const Footer = () => (
    <footer className="footer">
        <div className="container">
            <div className="footer-nav">
                <div className="single-item">
                    <h3 className="title">O firmie</h3>
                    <ul>
                        <ListItemLink>O nas</ListItemLink>
                        <ListItemLink>Dane przewoźnika</ListItemLink>
                        <ListItemLink>Dokumenty</ListItemLink>
                        <ListItemLink>Polityka prywatności</ListItemLink>
                        <ListItemLink>Kontakt</ListItemLink>
                    </ul>
                </div>
                <div className="single-item">
                    <h3 className="title">Zasoby</h3>
                    <ul>
                        <ListItem>
                            <Link to="/my-tickets">Moje bilety</Link>
                        </ListItem>
                        <ListItem>
                            <Link to="/terms">Regulamin</Link>
                        </ListItem>
                    </ul>
                </div>
                <div className="single-item">
                    <h3 className="title">Inne informacje</h3>
                    <ul>
                        <ListItemLink>Promocje i oferty</ListItemLink>
                        <ListItemLink>Pomoc</ListItemLink>
                        <ListItemLink>Opinie o nas</ListItemLink>
                    </ul>
                </div>
            </div>
            <div className="details">
                <div className="made-with">
                    <span>© 2020 Train Tickets</span>
                    <br />
                    <span>Made with ♥︎ in Zakopane, Poland.</span>
                </div>
                <div className="social">
                    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                    <a href="#">
                        <FontAwesomeIcon icon={faFacebookF} />
                    </a>
                    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                    <a href="#">
                        <FontAwesomeIcon icon={faTwitter} />
                    </a>
                    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                    <a href="#">
                        <FontAwesomeIcon icon={faInstagram} />
                    </a>
                </div>
            </div>
        </div>
    </footer>
)
