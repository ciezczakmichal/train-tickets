import React from 'react'
import { Link, useLocation } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import { useStore } from '../helpers/storeContext'
import logo from './../assets/logo.png'
import './Header.scss'

export const Header = () => {
    const store = useStore()
    const { pathname } = useLocation()
    const signedIn = store.isUserSignedIn()

    const handleSearchClick = event => {
        const mainPageVisible =
            pathname === '/' || pathname.startsWith('/search-results')

        if (mainPageVisible) {
            event.preventDefault()
        }
    }

    return (
        <header className="app-header">
            <div className="container">
                <div className="content">
                    <Link className="logo" to="/">
                        <img src={logo} alt="Logo Train Tickets" />
                    </Link>
                    <nav className="main-nav">
                        <ul>
                            <li>
                                <Link to="/" onClick={handleSearchClick}>
                                    Wyszukaj połączenie
                                </Link>
                            </li>
                            {signedIn && (
                                <li>
                                    <Link to="/my-tickets">Moje bilety</Link>
                                </li>
                            )}
                            {!signedIn && (
                                <li>
                                    <Link to="/sign">
                                        Zaloguj | Zarejestruj
                                    </Link>
                                </li>
                            )}
                            {signedIn && (
                                <li>
                                    <Link to="/" onClick={() => store.logOut()}>
                                        <span className="log-out-icon">
                                            <FontAwesomeIcon
                                                icon={faSignOutAlt}
                                            />
                                        </span>
                                        Wyloguj
                                    </Link>
                                </li>
                            )}
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
    )
}
