import { createContext, useContext } from 'react'
import { Store } from './store'

let store = null
let context = null

export const createStore = () => new Store()

export const createStoreContext = newStore => {
    store = newStore
    context = createContext()
    return context
}

export const getStoreContext = () => {
    if (!context) {
        throw new Error('Nie utworzono jeszcze kontekstu magazynu')
    }

    return context
}

// własny hook :)
export const useStore = () => {
    useContext(getStoreContext())
    return store
}
