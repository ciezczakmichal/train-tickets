export const currentDateForInput = () => {
    const date = new Date()

    let day = date.getDate()
    let month = date.getMonth() + 1
    const year = date.getFullYear()

    month = (month < 10 ? '0' : '') + month
    day = (day < 10 ? '0' : '') + day

    return `${year}-${month}-${day}`
}

export const currentTimeForInput = () => {
    const date = new Date()

    let hour = date.getHours()
    let min = date.getMinutes()

    hour = (hour < 10 ? '0' : '') + hour
    min = (min < 10 ? '0' : '') + min

    return `${hour}:${min}`
}
