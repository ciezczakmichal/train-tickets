import axios from 'axios'

const ApiUrl = process.env.REACT_APP_API_URL
const ApiUrlVariable = 'REACT_APP_API_URL'

const getEndpointUrl = endpoint => {
    if (!ApiUrl) {
        throw new Error(
            `Nie zdefiniowano adresu API (zmienna środowiskowa ${ApiUrlVariable})`
        )
    }

    return `${ApiUrl}/api/${endpoint}`
}

export class API {
    static news = null
    static staticPages = new Map()

    static async fetchStations() {
        const url = getEndpointUrl('stations')

        try {
            const response = await axios.get(url)
            return response.data
        } catch (error) {
            console.error(error)
            return null
        }
    }

    static async fetchTrainCategories() {
        const url = getEndpointUrl('train-categories')

        try {
            const response = await axios.get(url)
            return response.data
        } catch (error) {
            console.error(error)
            return null
        }
    }

    static async fetchTrains(params) {
        const url = getEndpointUrl('searchTrains')

        try {
            const response = await axios.post(url, params)
            return response.data
        } catch (error) {
            console.error(error)
            return null
        }
    }

    static async signIn(email, password) {
        const params = { email, password }
        const url = getEndpointUrl('users/signIn')

        try {
            const response = await axios.post(url, params)
            return response.data
        } catch (error) {
            console.error(error)
            return null
        }
    }

    static async signUp(email, password) {
        const params = { email, password }
        const url = getEndpointUrl('users/signUp')

        try {
            const response = await axios.post(url, params)
            return response.data
        } catch (error) {
            console.error(error)
            return null
        }
    }

    // zwraca true / false
    static async logOut(token) {
        const params = { token }
        const url = getEndpointUrl('users/logOut')

        try {
            await axios.post(url, params)
            return true
        } catch (error) {
            console.error(error)
            return false
        }
    }

    static async buyTicket(userToken, entryId, stationFromId, stationToId) {
        const params = {
            token: userToken,
            entryID: entryId,
            stationFromID: stationFromId,
            stationToID: stationToId,
        }

        const url = getEndpointUrl('tickets/buy')

        try {
            await axios.post(url, params)
            return true
        } catch (error) {
            console.error(error)
            return false
        }
    }

    static async fetchUserTickets(userToken) {
        const url = getEndpointUrl(`tickets/forUser/${userToken}`)

        try {
            const response = await axios.get(url)
            return response.data
        } catch (error) {
            console.error(error)
            return null
        }
    }

    static async fetchTicketQr(ticketID) {
        const url = getEndpointUrl(`tickets/${ticketID}/qr`)

        try {
            const response = await axios.get(url)
            return response.data
        } catch (error) {
            console.error(error)
            return null
        }
    }

    static async getNews() {
        // użyj wartości zbuforowanej
        if (this.news !== null) {
            return this.news
        }

        const url = getEndpointUrl('news')

        try {
            const response = await axios.get(url)
            this.news = response.data
            return this.news
        } catch (error) {
            console.error(error)
            return null
        }
    }

    static getNewsImageFullUrl(url) {
        return ApiUrl + url
    }

    static async getStaticPage(pageName) {
        // użyj wartości zbuforowanej
        if (this.staticPages.has(pageName)) {
            return this.staticPages.get(pageName)
        }

        const url = getEndpointUrl(`static/pages/${pageName}.html`)

        try {
            const response = await axios.get(url)
            const result = response.data
            this.staticPages.set(pageName, result)
            return result
        } catch (error) {
            console.error(error)
            return null
        }
    }
}
