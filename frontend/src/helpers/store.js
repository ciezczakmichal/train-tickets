export class Store {
    /* Wartość identyfikująca stan magazynu.
     * Zmiana wartości oznacza zmianę zawartości obiektu */

    value = 0

    wrapSetState(setState) {
        return (...args) => {
            this.value++
            setState(...args)
        }
    }

    /* Właściwości do podpięcia */

    stations = null
    setStations = null

    trainCategories = null
    setTrainCategories = null

    token = null
    setToken = null

    /* Metody podpinające */

    bindStations(data, setData) {
        this.stations = data
        this.setStations = this.wrapSetState(setData)
    }

    bindTrainCategories(data, setData) {
        this.trainCategories = data
        this.setTrainCategories = this.wrapSetState(setData)
    }

    bindToken(data, setData) {
        this.token = data
        this.setToken = this.wrapSetState(setData)
    }

    /* Stacje */

    getStationById(id) {
        return this.stations.find(s => s.id === id) || null
    }

    getStationIdFromName(name) {
        const found = this.stations.find(station => station.name === name)
        return found ? found.id : null
    }

    /* Kategorie pociągów */

    getTrainCategory(id) {
        return this.trainCategories.find(category => category.id === id) || null
    }

    getTrainCategoryFromName(name) {
        return (
            this.trainCategories.find(category => category.name === name) ||
            null
        )
    }

    /* Token zalogowanego użytkownika */

    isUserSignedIn() {
        return this.token !== null
    }

    logOut() {
        this.setToken(null)
    }
}
