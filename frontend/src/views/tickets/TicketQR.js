import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronCircleLeft } from '@fortawesome/free-solid-svg-icons'
import './TicketQR.scss'

export const TicketQR = props => {
    const { ticketData, qrData, onClose = () => {} } = props

    const imageSrc = `data:image/png;base64,${qrData}`

    return (
        <div className="ticket-qr">
            <div className="container">
                <h2 className="page-title">Kod QR biletu</h2>
                <div className="data">
                    <div className="description">
                        <span>Numer biletu: {ticketData.id}</span>
                    </div>
                    <img src={imageSrc} alt="Kod QR biletu" />
                </div>
                <div className="back-button">
                    <button
                        type="button"
                        className="back-to-tickets"
                        title="Wróć do listy biletów"
                        onClick={onClose}
                    >
                        <i>
                            <FontAwesomeIcon icon={faChevronCircleLeft} />
                        </i>
                        Wróć do listy biletów
                    </button>
                </div>
            </div>
        </div>
    )
}
