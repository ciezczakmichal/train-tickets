import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faQrcode } from '@fortawesome/free-solid-svg-icons'
import { TrainCategory } from '../../components/TrainCategory'
import { useStore } from '../../helpers/storeContext'
import { API } from '../../helpers/API'
import moment from '../../helpers/moment'
import { TicketQR } from './TicketQR'
import './index.scss'

const formatDate = value => {
    return moment(value).format('DD.MM.YYYY')
}
const formatTime = value => {
    return moment(value).format('HH:mm')
}

const Ticket = props => {
    const { data, onShowTicketQr = () => {} } = props
    const store = useStore()

    const getTravelStations = ticket => {
        return `${store.getStationById(ticket.stationFromID).displayName} - ${
            store.getStationById(ticket.stationToID).displayName
        }`
    }

    const category = store.getTrainCategory(data.trainCategoryID)

    return (
        <div className="ticket">
            <div className="train-category column-first">
                <TrainCategory selectable={false} category={category} />
            </div>
            <div className="travel-stations column-long">
                <span className="single">{getTravelStations(data)}</span>
            </div>
            <div className="departure-time column-short">
                <span className="multi">{formatDate(data.departureTime)}</span>
                <span className="multi">{formatTime(data.departureTime)}</span>
            </div>
            <div className="purchase-time column-short">
                <span className="multi">{formatDate(data.purchaseTime)}</span>
                <span className="multi">{formatTime(data.purchaseTime)}</span>
            </div>
            <div className="actions column-last">
                <button
                    type="button"
                    className="show-qr-button"
                    title="Wyświetl kod QR biletu"
                    onClick={onShowTicketQr}
                >
                    <i>
                        <FontAwesomeIcon icon={faQrcode} />
                    </i>
                    Pokaż QR
                </button>
            </div>
        </div>
    )
}

export const TicketsView = props => {
    const history = useHistory()
    const store = useStore()

    const signedIn = store.isUserSignedIn()

    const [tickets, setTickets] = useState(null)
    const [downloading, setDownloading] = useState(false)
    const [qrTicketData, setQrTicketData] = useState(null)
    const [qrData, setQrData] = useState(null)

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => {
        if (!signedIn || downloading || tickets !== null) {
            return
        }

        const getData = async () => {
            const data = await API.fetchUserTickets(store.token)

            if (data !== null) {
                setTickets(data)
                setDownloading(false)
            }
        }

        setDownloading(true)
        getData()
    })

    useEffect(() => {
        if (!signedIn) {
            history.push('/sign')
        }
    })

    // zakończ wcześnie rendering
    if (!signedIn || tickets === null) {
        return null
    }

    const handleShowQr = async ticket => {
        const data = await API.fetchTicketQr(ticket.id)

        if (data !== null) {
            setQrTicketData(ticket)
            setQrData(data)
        }
    }

    const handleCloseQrView = () => {
        setQrTicketData(null)
        setQrData(null)
    }

    const qrVisible = qrData !== null
    const entries = tickets.map(ticket => (
        <Ticket
            key={ticket.id}
            data={ticket}
            onShowTicketQr={() => handleShowQr(ticket)}
        />
    ))

    return (
        <section className="my-tickets">
            {qrVisible ? (
                <TicketQR
                    ticketData={qrTicketData}
                    qrData={qrData}
                    onClose={handleCloseQrView}
                />
            ) : (
                <div className="container">
                    <h2 className="page-title">Moje bilety</h2>
                    <div className="tickets-list">
                        <div className="header">
                            <div className="train-category column-first"></div>
                            <div className="travel-stations column-long">
                                <span className="single">Relacja</span>
                            </div>
                            <div className="departure-time column-short">
                                <span className="single">
                                    Data i czas wyjazdu
                                </span>
                            </div>
                            <div className="purchase-time column-short">
                                <span className="single">
                                    Data i czas zakupu
                                </span>
                            </div>
                            <div className="actions column-last">
                                <span className="single">Akcje</span>
                            </div>
                        </div>
                        <div className="body">{entries}</div>
                    </div>
                </div>
            )}
        </section>
    )
}
