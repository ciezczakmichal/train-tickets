import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faExclamationCircle,
    faSignInAlt,
    faUserPlus,
} from '@fortawesome/free-solid-svg-icons'
import { TermsCheckbox } from './TermsCheckbox'
import { API } from '../../helpers/API'
import { useStore } from '../../helpers/storeContext'
import './index.scss'

const SignInTabName = 'sign-in'
const SignUpTabName = 'sign-up'

export const SignView = () => {
    const history = useHistory()
    const store = useStore()

    const [tabName, setTabName] = useState(SignInTabName)
    const [validationOn, setValidationOn] = useState(false)

    const [signInEmail, setSignInEmail] = useState('')
    const [signInPassword, setSignInPassword] = useState('')

    const [signUpEmail, setSignUpEmail] = useState('')
    const [signUpPassword, setSignUpPassword] = useState('')
    const [signUpPassword2, setSignUpPassword2] = useState('')
    const [signUpConfirmed, setSignUpConfirmed] = useState(false)

    const [signInErrorBox, setSignInErrorBox] = useState(false)
    const [signUpErrorBox, setSignUpErrorBox] = useState(false)

    const isCurrentTab = requstedTabName => tabName === requstedTabName

    const tabClassesFor = requstedTabName => {
        const active = isCurrentTab(requstedTabName) ? ' active' : ''
        return 'tab' + active
    }

    const contentClassesFor = baseClassName => {
        const extra = validationOn ? ' validate' : ''
        return baseClassName + extra
    }

    const handleSignIn = event => {
        event.preventDefault()

        const executeAction = async () => {
            const token = await API.signIn(signInEmail, signInPassword)

            if (token === null) {
                setSignInErrorBox(true)
            } else {
                setSignInErrorBox(false)
                store.setToken(token)
                history.push('/')
            }
        }

        executeAction()
    }

    const handleSignUp = event => {
        event.preventDefault()

        // wymagana akceptacja regulaminu
        if (!signUpConfirmed) {
            return
        }

        if (signUpPassword !== signUpPassword2) {
            alert('Hasła się różnią!')
            return
        }

        const executeAction = async () => {
            const token = await API.signUp(signUpEmail, signUpPassword)

            if (token === null) {
                setSignUpErrorBox(true)
            } else {
                setSignUpErrorBox(false)
                store.setToken(token)
                history.push('/')
            }
        }

        executeAction()
    }

    const handleValidateForm = () => {
        setValidationOn(true)
    }

    const window = (
        <div className="window">
            <div className="tabs">
                <div
                    className={tabClassesFor(SignInTabName)}
                    onClick={() => setTabName(SignInTabName)}
                >
                    Logowanie
                </div>
                <div
                    className={tabClassesFor(SignUpTabName)}
                    onClick={() => setTabName(SignUpTabName)}
                >
                    Rejestracja
                </div>
            </div>
            <div className="content">
                {isCurrentTab(SignInTabName) && (
                    <form
                        className={contentClassesFor('sign-in')}
                        onSubmit={handleSignIn}
                    >
                        {signInErrorBox && (
                            <div className="error-box">
                                <div className="icon-ctr">
                                    <i>
                                        <FontAwesomeIcon
                                            icon={faExclamationCircle}
                                        />
                                    </i>
                                </div>
                                <div>
                                    <span className="error-info">
                                        Próba logowania nie powiodła się.
                                        Upewnij się, że dane są poprawne.
                                    </span>
                                </div>
                            </div>
                        )}
                        <label htmlFor="sign-in-email">E-mail</label>
                        <input
                            value={signInEmail}
                            type="email"
                            id="sign-in-email"
                            title="Podaj swój e-mail"
                            placeholder="Podaj swój e-mail"
                            required
                            onChange={event =>
                                setSignInEmail(event.target.value)
                            }
                        />
                        <label htmlFor="sign-in-password">Hasło</label>
                        <input
                            value={signInPassword}
                            type="password"
                            id="sign-in-password"
                            title="Wpisz hasło"
                            placeholder="Wpisz hasło"
                            minLength="6"
                            required
                            onChange={event =>
                                setSignInPassword(event.target.value)
                            }
                        />
                        <div className="button-ctr">
                            <button
                                type="submit"
                                className="action"
                                title="Zaloguj się"
                                onClick={handleValidateForm}
                            >
                                <i>
                                    <FontAwesomeIcon icon={faSignInAlt} />
                                </i>
                                Zaloguj się
                            </button>
                        </div>
                    </form>
                )}
                {isCurrentTab(SignUpTabName) && (
                    <form
                        className={contentClassesFor('sign-up')}
                        onSubmit={handleSignUp}
                    >
                        {signUpErrorBox && (
                            <div className="error-box">
                                <div className="icon-ctr">
                                    <i>
                                        <FontAwesomeIcon
                                            icon={faExclamationCircle}
                                        />
                                    </i>
                                </div>
                                <div>
                                    <span className="error-info">
                                        Rejestracja nie powiodła się, być może
                                        takie konto już istnieje. Sprawdź
                                        poprawność danych.
                                    </span>
                                </div>
                            </div>
                        )}
                        <label htmlFor="sign-up-email">E-mail</label>
                        <input
                            value={signUpEmail}
                            type="email"
                            id="sign-up-email"
                            title="Podaj swój e-mail"
                            placeholder="Podaj swój e-mail"
                            required
                            onChange={event =>
                                setSignUpEmail(event.target.value)
                            }
                        />
                        <label htmlFor="sign-up-password">Hasło</label>
                        <input
                            value={signUpPassword}
                            type="password"
                            id="sign-up-password"
                            title="Wpisz hasło"
                            placeholder="Wpisz hasło"
                            minLength="6"
                            required
                            onChange={event =>
                                setSignUpPassword(event.target.value)
                            }
                        />
                        <label htmlFor="sign-up-password2">Powtórz hasło</label>
                        <input
                            value={signUpPassword2}
                            type="password"
                            id="sign-up-password2"
                            title="Powtórz hasło"
                            placeholder="Powtórz hasło"
                            minLength="6"
                            required
                            onChange={event =>
                                setSignUpPassword2(event.target.value)
                            }
                        />
                        <div>
                            <TermsCheckbox
                                checked={signUpConfirmed}
                                className={
                                    validationOn && !signUpConfirmed
                                        ? 'invalid'
                                        : ''
                                }
                                onChange={value => setSignUpConfirmed(value)}
                            />
                        </div>
                        <div className="button-ctr">
                            <button
                                type="submit"
                                className="action"
                                title="Zarejestruj się"
                                onClick={handleValidateForm}
                            >
                                <i>
                                    <FontAwesomeIcon icon={faUserPlus} />
                                </i>
                                Zarejestruj się
                            </button>
                        </div>
                    </form>
                )}
            </div>
        </div>
    )

    return (
        <section className="sign-page background">
            <div className="container">
                <div className="window-ctr">{window}</div>
            </div>
        </section>
    )
}
