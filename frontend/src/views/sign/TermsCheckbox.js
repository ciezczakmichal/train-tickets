import React from 'react'
import { Link } from 'react-router-dom'
import './TermsCheckbox.scss'

export const TermsCheckbox = props => {
    const { checked = false, className = '', onChange = () => {} } = props

    const handleChange = event => {
        onChange(event.target.checked)
    }

    return (
        <div className={`checkbox ${className}`}>
            <input
                type="checkbox"
                id="checkbox"
                checked={checked}
                onChange={handleChange}
                required
            />
            <label htmlFor="checkbox">
                Oświadczam, iż zapoznałem się z postanowieniami{' '}
                <Link to="/terms" target="_blank">
                    regulaminu i polityką przetwarzania danych
                </Link>
                .
            </label>
        </div>
    )
}
