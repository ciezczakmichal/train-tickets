import React, { useState, useEffect } from 'react'
import './index.scss'
import { API } from '../../helpers/API'

export const TermsView = () => {
    const [content, setContent] = useState(null)
    const [downloading, setDownloading] = useState(false)

    useEffect(() => {
        if (downloading || content) {
            return
        }

        const getData = async () => {
            const data = await API.getStaticPage('terms')
            setContent(data)

            if (data) {
                setDownloading(false)
            }
        }

        setDownloading(true)
        getData()
    }, [downloading, content, setDownloading])

    const html = { __html: content }

    return (
        <section className="terms-view">
            <div className="container">
                <h2 className="page-title">
                    Regulamin korzystania z aplikacji
                </h2>
                <div className="content" dangerouslySetInnerHTML={html} />
            </div>
        </section>
    )
}
