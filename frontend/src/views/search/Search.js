import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExchangeAlt, faSearch } from '@fortawesome/free-solid-svg-icons'
import { StationSelect } from '../../components/StationSelect'
import { TrainCategory } from '../../components/TrainCategory'
import { useStore } from '../../helpers/storeContext'
import {
    currentDateForInput,
    currentTimeForInput,
} from '../../helpers/currentDateTime'
import './Search.scss'

export const Search = () => {
    const store = useStore()
    const { stations, trainCategories } = store
    const allCategoriesId = trainCategories.map(category => category.id)

    const history = useHistory()

    const [stationFrom, setStationFrom] = useState(null)
    const [stationTo, setStationTo] = useState(null)
    const [date, setDate] = useState(currentDateForInput())
    const [time, setTime] = useState(currentTimeForInput())
    const [departure, setDeparture] = useState(true)

    // wybierz domyślnie wszystkie kategorie
    const [selectedCategories, setSelectedCategories] = useState(
        allCategoriesId
    )

    const [stationFromValid, setStationFromValid] = useState(true)
    const [stationToValid, setStationToValid] = useState(true)

    const [executeSearch, setExecuteSearch] = useState(false)

    const categorySelected = categoryId => {
        return selectedCategories.includes(categoryId)
    }

    const updateStationsValidity = () => {
        const same = stationFrom === stationTo

        setStationFromValid(!same && stationFrom !== null)
        setStationToValid(!same && stationTo !== null)
    }

    const searchRoutes = () => {
        if (!stationFromValid || !stationToValid) {
            return
        }

        let categories = selectedCategories

        // jeśli brak zaznaczonych kategorii, zaznacz je z powrotem
        if (categories.length === 0) {
            setSelectedCategories(allCategoriesId)
            categories = selectedCategories
        }

        const from = store.getStationById(stationFrom).name
        const to = store.getStationById(stationTo).name
        const departureStr = departure.toString() // bool na string
        const catgsNames = categories
            .map(id => store.getTrainCategory(id).name)
            .join(',')

        // /search-results/:from/:to/:date/:time/:departure/:categories
        history.push(
            `/search-results/${from}/${to}/${date}/${time}/${departureStr}/${catgsNames}`
        )
    }

    const handleSubmit = event => {
        event.preventDefault()

        updateStationsValidity()
        setExecuteSearch(true)
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => {
        if (executeSearch) {
            searchRoutes()
            setExecuteSearch(false)
        }
    })

    const handleReverseStations = () => {
        const newTo = stationFrom
        setStationFrom(stationTo)
        setStationTo(newTo)
    }

    const handleSelectCategory = (categoryId, selected) => {
        const index = selectedCategories.findIndex(item => item === categoryId)
        const added = index > -1

        let newSelection = selectedCategories

        if (selected && !added) {
            newSelection = [...selectedCategories, categoryId]
        } else if (!selected && added) {
            newSelection = selectedCategories.filter(id => id !== categoryId)
        }

        setSelectedCategories(newSelection)
    }

    const categories = trainCategories.map(category => (
        <TrainCategory
            key={category.id}
            category={category}
            selected={categorySelected(category.id)}
            onSelected={selected => handleSelectCategory(category.id, selected)}
        />
    ))

    return (
        <section className="train-search background">
            <div className="container">
                <div className="content-wrapper">
                    <div className="content">
                        <h1 className="main-title">
                            Wyszukaj połączenie i kup bliet
                        </h1>
                        <h2 className="desc">
                            Oferujemy atrakcyjne połączenia na liniach{' '}
                            <span className="desc-cities">
                                Zakopane - Kraków
                            </span>{' '}
                            oraz{' '}
                            <span className="desc-cities">
                                Rzeszów - Przemyśl
                            </span>
                        </h2>
                        <form
                            className="train-search-form"
                            onSubmit={handleSubmit}
                        >
                            <div className="inputs-block">
                                <div className="station-params">
                                    <StationSelect
                                        stationId={stationFrom}
                                        stations={stations}
                                        placeholder="Wyjazd z..."
                                        title="Wpisz nazwę stacji odjazdu"
                                        invalid={!stationFromValid}
                                        onChange={id => setStationFrom(id)}
                                    />
                                    <div className="swap-stations">
                                        <button
                                            type="button"
                                            title="Zamień stacje"
                                            onClick={handleReverseStations}
                                        >
                                            <FontAwesomeIcon
                                                icon={faExchangeAlt}
                                            />
                                        </button>
                                    </div>
                                    <StationSelect
                                        stationId={stationTo}
                                        stations={stations}
                                        placeholder="Przyjazd do..."
                                        title="Wpisz nazwę stacji przyjazdu"
                                        invalid={!stationToValid}
                                        onChange={id => setStationTo(id)}
                                    />
                                </div>
                                <div className="datetime-params">
                                    <div className="input-wrapper">
                                        <label
                                            htmlFor="search-date"
                                            className="input-title"
                                        >
                                            Data
                                        </label>
                                        <input
                                            type="date"
                                            name="search-date"
                                            id="search-date"
                                            className="search-date"
                                            title="Podaj datę odjazdu lub przyjazdu"
                                            autoComplete="off"
                                            required
                                            value={date}
                                            onChange={event =>
                                                setDate(event.target.value)
                                            }
                                        />
                                    </div>
                                    <div className="input-wrapper">
                                        <label
                                            htmlFor="search-time"
                                            className="input-title"
                                        >
                                            Godzina
                                        </label>
                                        <input
                                            type="time"
                                            name="search-time"
                                            id="search-time"
                                            className="search-time"
                                            title="Podaj godzinę odjazdu lub przyjazdu"
                                            autoComplete="off"
                                            required
                                            value={time}
                                            onChange={event =>
                                                setTime(event.target.value)
                                            }
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="settings">
                                <div className="departure-setting">
                                    {/*  eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                                    <a
                                        className={departure ? 'selected' : ''}
                                        onClick={() => setDeparture(true)}
                                    >
                                        Odjazd
                                    </a>
                                    {/*  eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                                    <a
                                        className={!departure ? 'selected' : ''}
                                        onClick={() => setDeparture(false)}
                                    >
                                        Przyjazd
                                    </a>
                                </div>
                                <div className="train-categories">
                                    {categories}
                                </div>
                            </div>
                            <div className="search-btn-ctr">
                                <button
                                    type="submit"
                                    className="search-btn"
                                    title="Kliknij, aby wyszukać połączenia"
                                >
                                    <i>
                                        <FontAwesomeIcon icon={faSearch} />
                                    </i>
                                    Szukaj
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    )
}
