import React, { useState, useEffect } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowCircleRight } from '@fortawesome/free-solid-svg-icons'
import { API } from '../../helpers/API'
import './NewsFeed.scss'

const News = props => {
    const { imageSrc, imageAlt, title, description, url } = props

    return (
        <article className="single-item">
            <div className="image">
                <img src={imageSrc} alt={imageAlt} />
            </div>
            <h3 className="title">{title}</h3>
            <p
                className="desc"
                dangerouslySetInnerHTML={{ __html: description }}
            />
            <div className="more-btn-ctr">
                <a href={url} className="more-btn">
                    <span>Zobacz więcej</span>
                    <span className="icon">
                        <FontAwesomeIcon icon={faArrowCircleRight} />
                    </span>
                </a>
            </div>
        </article>
    )
}

export const NewsFeed = () => {
    const [news, setNews] = useState([])
    const [downloading, setDownloading] = useState(false)

    useEffect(() => {
        if (downloading || news.length > 0) {
            return
        }

        const getData = async () => {
            const data = await API.getNews()

            if (Array.isArray(data)) {
                setNews(data)
                setDownloading(false)
            }
        }

        setDownloading(true)
        getData()
    }, [downloading, news, setDownloading])

    const articles = news.map(article => (
        <News
            key={article.id}
            imageSrc={API.getNewsImageFullUrl(article.image)}
            imageAlt={article.image_alt}
            title={article.title}
            description={article.description}
            url={article.url}
        />
    ))

    return (
        articles.length > 0 && (
            <section className="news-section">
                <div className="container">
                    <h2 className="section-title">Aktualności</h2>
                    <div className="news-list">{articles}</div>
                </div>
            </section>
        )
    )
}
