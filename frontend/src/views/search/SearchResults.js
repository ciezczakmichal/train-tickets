import React, { useEffect, useState, useRef } from 'react'
import { useParams, useLocation, useHistory } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faChevronRight,
    faAngleDoubleLeft,
    faAngleDoubleRight,
    faCreditCard,
} from '@fortawesome/free-solid-svg-icons'
import { TrainCategory } from '../../components/TrainCategory'
import moment from './../../helpers/moment'
import { useStore } from '../../helpers/storeContext'
import { API } from '../../helpers/API'
import { cloneObject } from '../../helpers/cloneObject'
import './SearchResults.scss'

const formatTime = input => {
    return moment(input).format('HH:mm')
}

const formatTravelTime = minutes => {
    const hours = Math.trunc(minutes / 60)
    let min = minutes % 60
    min = min < 10 ? `0${min}` : min

    return `${hours}h${min}`
}

const formatPrice = value => {
    return `${value.toLocaleString('pl')} zł`
}

const DayBlock = props => {
    const { date, trains, searchParams } = props
    const store = useStore()
    const history = useHistory()

    const handleBuy = async trainId => {
        if (!store.isUserSignedIn()) {
            history.push('/sign')
            return
        }

        const success = await API.buyTicket(
            store.token,
            trainId,
            searchParams.stationFromID,
            searchParams.stationToID
        )

        if (success) {
            history.push('/my-tickets')
        } else {
            alert('Wystąpił błąd podczas zakupu tego biletu.')
        }
    }

    const entries = trains.map((train, index) => {
        const {
            id,
            departureTime,
            arrivalTime,
            routeInfo,
            travelTime,
            price,
            purchasable,
        } = train

        const category = store.getTrainCategory(routeInfo.trainCatID)

        return (
            <div className="result-item" key={index}>
                <div className="departure-time">
                    <span className="single">{formatTime(departureTime)}</span>
                </div>
                <div className="arrival-time">
                    <span className="single">{formatTime(arrivalTime)}</span>
                </div>
                <div className="route-info">
                    <TrainCategory selectable={false} category={category} />
                    <span className="multi">{routeInfo.trainID}</span>
                </div>
                <div className="travel-time">
                    <span className="single">
                        {formatTravelTime(travelTime)}
                    </span>
                </div>
                <div className="price">
                    <span className="single">{formatPrice(price)}</span>
                </div>
                <div className="small-space">
                    {purchasable && (
                        <button
                            type="button"
                            className="buy-button upper"
                            title="Kliknij, aby dokonać zakupu biletu"
                            onClick={() => handleBuy(id)}
                        >
                            <i>
                                <FontAwesomeIcon icon={faCreditCard} />
                            </i>
                            ZAKUP
                        </button>
                    )}
                </div>
            </div>
        )
    })

    return (
        <div className="day-group">
            <div className="single-row date-info-ctr">
                <span className="date-info">{date}</span>
            </div>
            {entries}
        </div>
    )
}

const decodeUrlParams = (urlParams, store) => {
    const { from, to, date, time, departure, categories } = urlParams

    const decodeTrainCategories = data => {
        const names = data.split(',')
        return names
            .map(name => store.getTrainCategoryFromName(name))
            .filter(category => category !== null)
            .map(category => category.id)
    }

    return {
        stationFromID: store.getStationIdFromName(from),
        stationToID: store.getStationIdFromName(to),
        date,
        time,
        departure: departure === 'true',
        trainCategories: decodeTrainCategories(categories),
    }
}

const resultsByDate = response => {
    const result = new Map()

    response.results.forEach(item => {
        const date = moment(item.departureTime).format('dddd, Do MMMM')

        if (result.has(date)) {
            result.set(date, [...result.get(date), item])
        } else {
            result.set(date, [item])
        }
    })

    return result
}

const scrollToRef = ref =>
    ref.current.scrollIntoView({ behavior: 'smooth', block: 'start' })

export const SearchResults = () => {
    const params = useParams()
    const location = useLocation()
    const store = useStore()

    const [searchKey, setSearchKey] = useState('')
    const [downloading, setDownloading] = useState(false)
    const [result, setResult] = useState(null)

    const scrollRef = useRef(null)

    const fetchData = async () => {
        // konwersja danych z parametrów
        const requestParams = decodeUrlParams(params, store)
        const data = await API.fetchTrains(requestParams)

        if (data !== null) {
            setResult(data)
            setDownloading(false)

            scrollToRef(scrollRef)
        }
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => {
        if ((downloading || result !== null) && location.key === searchKey) {
            return
        }

        if (!downloading) {
            setSearchKey(location.key)
            setDownloading(true)
            fetchData()
        }
    })

    const handleFetchMore = async later => {
        if (result === null) {
            return
        }

        const requestParams = decodeUrlParams(params, store)
        const index = later ? result.results.length - 1 : 0
        const obj = result.results[index]

        requestParams.date = moment(obj.departureTime)
            .clone()
            .add(later ? 1 : -1, 'days')
            .format('YYYY-MM-DD')
        requestParams.time = '00:00'

        const data = await API.fetchTrains(requestParams)

        if (data === null) {
            return
        }

        setResult(prevResult => {
            const newResult = cloneObject(prevResult)

            if (later) {
                newResult.results = prevResult.results.concat(data.results)
            } else {
                newResult.results = data.results.concat(newResult.results)
            }

            return newResult
        })
    }

    const resultAvailable = !downloading && result !== null
    let view = null

    if (!resultAvailable) {
        view = (
            <div className="loading-data">
                <span>Trwa wyszukiwanie połączeń, proszę czekać...</span>
            </div>
        )
    } else {
        const items = []

        for (const [date, trains] of resultsByDate(result)) {
            items.push(
                <DayBlock
                    key={date}
                    date={date}
                    trains={trains}
                    searchParams={result.params}
                />
            )
        }

        const stationFrom = store.getStationById(result.params.stationFromID)
        const stationTo = store.getStationById(result.params.stationToID)

        const noData = resultAvailable && result.results.length === 0

        view = (
            <>
                <div className="scroll-ref" ref={scrollRef} />
                <div className="search-title">
                    <span className="keyword">Połączenia</span>
                    <div className="stations">
                        <span className="station-from">
                            {stationFrom.displayName}
                        </span>
                        <span className="separator">
                            <FontAwesomeIcon icon={faChevronRight} />
                        </span>
                        <span className="station-to">
                            {stationTo.displayName}
                        </span>
                    </div>
                </div>
                <div className="results-table">
                    <div className="header">
                        <div className="departure-time">
                            <span className="single">Czas odjazdu</span>
                        </div>
                        <div className="arrival-time">
                            <span className="single">Czas przyjazdu</span>
                        </div>
                        <div className="route-info">
                            <span className="multi">Rodzaj połączenia</span>
                            <span className="multi">Numer pociągu</span>
                        </div>
                        <div className="travel-time">
                            <span className="single">Czas podróży</span>
                        </div>
                        <div className="price">
                            <span className="single">Cena biletu</span>
                        </div>
                        <div className="small-space"></div>
                    </div>
                    <div className="body">
                        {noData ? (
                            <div className="no-data">
                                <span>
                                    Brak wyników, spróbuj wyszukać z innymi
                                    parametrami.
                                </span>
                            </div>
                        ) : (
                            <div>
                                <div className="single-row">
                                    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                                    <a
                                        className="more-results results-before"
                                        onClick={() => handleFetchMore(false)}
                                    >
                                        <span className="icon">
                                            <FontAwesomeIcon
                                                icon={faAngleDoubleLeft}
                                            />
                                        </span>
                                        Wcześniej
                                    </a>
                                </div>
                                <div className="results">{items}</div>
                                <div className="single-row">
                                    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                                    <a
                                        className="more-results results-after"
                                        onClick={() => handleFetchMore(true)}
                                    >
                                        <span className="icon">
                                            <FontAwesomeIcon
                                                icon={faAngleDoubleRight}
                                            />
                                        </span>
                                        Później
                                    </a>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </>
        )
    }

    return (
        <section className="search-results">
            <div className="container">{view}</div>
        </section>
    )
}
