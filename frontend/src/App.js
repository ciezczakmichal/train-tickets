import React, { useState, useEffect } from 'react'
import { Switch, Route } from 'react-router-dom'
import { Header } from './components/Header'
import { Footer } from './components/Footer'
import { TermsView } from './views/terms'
import { Search } from './views/search/Search'
import { NewsFeed } from './views/search/NewsFeed'
import { SearchResults } from './views/search/SearchResults'
import { SignView } from './views/sign'
import { TicketsView } from './views/tickets'
import { createStore, createStoreContext } from './helpers/storeContext'
import { API } from './helpers/API'

import './App.scss'

const store = createStore()
const StoreContext = createStoreContext(store)

const Core = () => (
    <div className="app">
        <Header />
        <main className="main">
            <Switch>
                <Route path="/my-tickets" exact>
                    <TicketsView />
                </Route>
                <Route path="/sign" exact>
                    <SignView />
                </Route>
                <Route path="/terms" exact>
                    <TermsView />
                </Route>
                <Route path="/">
                    <Search />
                    <Switch>
                        <Route
                            path="/search-results/:from/:to/:date/:time/:departure/:categories"
                            exact
                        >
                            <SearchResults />
                        </Route>
                        <Route>
                            <NewsFeed />
                        </Route>
                    </Switch>
                </Route>
            </Switch>
        </main>
        <Footer />
    </div>
)

export const App = () => {
    const [stations, setStations] = useState([])
    const [trainCategories, setTrainCategories] = useState([])
    const [token, setToken] = useState(null)

    const [downloading, setDownloading] = useState(false)

    store.bindStations(stations, setStations)
    store.bindTrainCategories(trainCategories, setTrainCategories)
    store.bindToken(token, setToken)
    // po przechwyceniu funkcji wszystkie zmiany powinny być wykonywane za pomocą store

    useEffect(() => {
        if (downloading || stations.length > 0) {
            return
        }

        const getData = async () => {
            let stationsData = API.fetchStations()
            const categoriesData = await API.fetchTrainCategories()
            stationsData = await stationsData

            if (Array.isArray(stationsData) && Array.isArray(categoriesData)) {
                store.setStations(stationsData)
                store.setTrainCategories(categoriesData)
                setDownloading(false)
            }
        }

        setDownloading(true)
        getData()
    }, [downloading, stations])

    const dataReady =
        !downloading && stations.length > 0 && trainCategories.length > 0

    return (
        <StoreContext.Provider value={store.value}>
            {dataReady && <Core />}
        </StoreContext.Provider>
    )
}
